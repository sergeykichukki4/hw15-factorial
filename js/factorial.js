"use strict";

let num;

do {
     num = prompt("Enter number");
} while (!num || isNaN(num) || !Number.isInteger(+num) || +num < 0)

function factorial(num) {
    if(+num === 0 || num === 1) {
        return 1;
    } else {
        return num * factorial(num - 1);
    }
}

console.log(factorial(num));
